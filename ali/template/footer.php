<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="index.css">
</head>
<footer>
    <nav id="navigation"></nav>
    <div class="footermenu">
    <div class="ABOUT">
        <h2>ABOUT US</h2>
        <p class="soz">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>       
        <div class="f_lakatsiya">
            <img src="./icons/icons8-маркер-24.png" alt="">
            <p>Zarafshon/shahar..... </p>
        </div>    
        <div class="f_tel">
            <img src="./icons/icons8-телефон-24.png" alt="">
            <p>+998933333333</p>
        </div>  
        <div class="f_email">
            <img src="./icons/icons8-открыть-электронное-письмо-24.png" alt="">
            <p>jnoiwevnronc@gmail.com</p>
        </div>
    </div>
    <div class="CATEGORIES">
        <h2>CATEGORIES</h2>
        <p>Hot deals</p>
        <p>Laptops</p>
        <p>Smartphones</p>
        <p>Cameras</p>
        <p>Accessories</p>
    </div>
    <div class="INFORMATION">
        <h2>INFORMATION</h2>
        <p>About Us</p>
        <p>Contact Us</p>
        <p>Privacy Policy</p>
        <p>Orders and Returns</p>
        <p>Terms & Conditions</p> 
    </div>
    <div class="SERVICE">
        <h2>SERVICE</h2>
        <p>My Account</p>
        <p>View Cart</p>
        <p>Wishlist</p>
        <p>Track My Order</p>
        <p>Help</p>
    </div>
    </div>
        <div class="container">
            <i class="fa fa-apple" id="apple"></i>
            <i class="fa fa-twitter" id="twitter"></i>
            <i class="fa fa-github-square github" id="github"></i>
            <i class="fa fa-facebook-square" id="facebook"></i>
        </div>
</footer>