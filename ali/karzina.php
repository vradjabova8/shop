<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shopping cart </title>
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
    <link rel="stylesheet" href="./style/karzina.css">
    <script src="./js/karzina.js"></script>
    <script>$(document).ready(function(){
        $(".size_select").click(function () {
           $(".slider").addClass("active");
           $(".select_size").show();
           $(".select_quantity").hide();
        })

        $(".qty_select").click(function () {
           $(".slider").addClass("active");
           $(".select_size").hide();
           $(".select_quantity").show();
        })

        $(".close, .bg_shadow, .button").click(function () {
           $(".slider").removeClass("active");
        })

        $(".select_size .body ul li").click(function () {
            $(".select_size .body ul li").removeClass("active");
           $(this).addClass("active");
           $(".size_select span").text($(this).text());
           $(".slider").removeClass("active");
        })

        $(".select_quantity .body ul li").click(function () {
            $(".select_quantity .body ul li").removeClass("active");
           $(this).addClass("active");
           $(".qty_select span").text($(this).text());
           $(".slider").removeClass("active");
        })

    });
    </script>
    <!-- <?php require("template/header.php") ?> -->
    <div class="cart_navigation">
        <a href="./index.php"><h1>HOME</h1></a>
        <div class="cart_logo">
            <a href="#"><i class="fas fa-shopping-cart"></i></a>
        </div>
        <div class="middle_content">
            <div class="bag">BAG---- </div>
            
            <div class="address">  ADDRESS----</div>
            
            <div class="payment">PAYMENT</div>
            
        </div>
        <div class="secure">
            <i class="fas fa-lock"></i>
            <p>100% secure</p>
        </div>
    </div>
    <div class="wrapper">

        <div class="wrapper_content">
            <div class="header_title">
                <div class="title">
                    MY SHOPPING CART :
                </div>
                <div class="amount">
                    <b>ITEMS : 2</b> 
                </div>

            </div>
            <div class="product_wrap">
                <div class="product_info">
                    <div class="product_img">
                        <img src="./img/shoe.gif" alt="ProductImage" width="200px" height="200px">
                    </div>
                    <div class="product_data">
                        <div class="description">
                            <div class="main_header">
                                Рroduct name :
                            </div>
                            <div class="sub_header">
                                Product Description 
                            </div>
                        </div>
                        <div class="qty">
                            <div class="size_select">
                                <p>Size : <span>9</span></p>
                            </div>
                            <div class="qty_select">
                                <p>Qty : <span>1</span></p>
                            </div>
                        </div>
                        <div class="price">Price : 15.000</div>
                    </div>
                </div>
                <div class="product_btns">
                    <div class="remove">BUY</div>
                    <div class="whishlist">FAVORIEST</div>
                </div>
                
            </div>
             <div class="product_wrap">
                <div class="product_info">
                    <div class="product_img">
                        <img src="./img/shoe.gif" alt="ProductImage" width="200px" height="200px">
                    </div>
                    <div class="product_data">
                        <div class="description">
                            <div class="main_header">
                                Рroduct name :
                            </div>
                            <div class="sub_header">
                                Product Description 
                            </div>
                        </div>
                        <div class="qty">
                            <div class="size_select">
                                <p>Size : <span>9</span></p>
                            </div>
                            <div class="qty_select">
                                <p>Qty : <span>1</span></p>
                            </div>
                        </div>
                        <div class="price">Price : 15.000</div>
                    </div>
                </div>
                <div class="product_btns">
                    <div class="remove">BUY</div>
                    <div class="whishlist">FAVORIEST</div>
                </div>
                
            </div>
            <div class="slider">
                <div class="bg_shadow"></div>
                <div class="select_size">
                    <div class="header">
                        <div class="slider_title">Select quanlity</div>
                        <div class="close">X</div>
                    </div>
                    <div class="body">
                        <ul>
                            <li>1</li>
                            <li>2</li>
                            <li>3</li>
                            <li>4</li>
                            <li>5</li>
                            <li>6</li>
                            <li>7</li>
                            <li>8</li>
                            <li class="active">9</li>
                            <li>10</li>
                        </ul>
                    </div>
                    <div class="footer">
                        <div class="button">DONE</div>
                    </div>
                </div>
                <div class="select_quantity">
                    <div class="header">
                        <div class="slider_title">Select Quantity</div>
                        <div class="close">X</div>
                    </div>
                    <div class="body">
                        <ul>
                            <li class="active">1</li>
                            <li>2</li>
                            <li>3</li>
                            <li>4</li>
                            <li>5</li>
                            <li>6</li>
                            <li>7</li>
                            <li>8</li>
                            <li>9</li>
                            <li>10</li>
                        </ul>
                    </div>
                    <div class="footer">
                        <div class="button">DONE</div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="wrapper_amount">
            <div class="header_title">
                <div class="title">
                    TOTAL PRICE DETAILS:
                </div>
                <div class="amount">
                    <b>25.000</b> 
                </div>
            </div>
            <div class="price_details">
                <div class="item">
                    <p>Total Amount:</p>
                    <p>30.000</p>
                </div>
                <div class="item">
                    <p>Bag Discount :</p>
                    <p class="green">-2000</p>
                </div>
                
                <div class="item">
                    <p>Order Total :</p>
                    <p>30.000</p>
                </div>
                <div class="item">
                    <p>Delivery Charges :</p>
                    <p><span style="text-decoration: line-through;"></span>5.000 <span class="green">FREE</span></p>
                </div>
                <div class="coupon">
                    <p>Coupon Discount :</p>
                    <p><a href="#">Apply Coupon</a></p>
                </div>
                <div class="total">
                    <p>Total :</p>
                    <p>33.000</p>
                </div>
            </div>
            <div class="checkout">
                <a href="#" role="button" class="checkout_btn">Place Order</a>
            </div>
        </div>
    </div>
    <!-- <?php require("template/footer.php"); ?> -->
</body>

</html>
</html>