<?php
	require ($_SERVER["DOCUMENT_ROOT"] . '/Ali/db.php');
if (isset($_POST['send_mes'])) {

	$errors = array();
	$done = array();
	$email = $_POST['email'];

	if ( trim($_POST['email']) == '' )
	{
		$errors[] = 'Enter your email!'; 
	}else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
		$errors[] = "Mail address entered incorrectly!";
	}
	if (count($errors) == 0) {
		$res_pass = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		            $shfl = str_shuffle($res_pass);
		            $res_pass = substr($shfl,0,8);
		$changemode = R::findOne( 'newusers', ' email = ? ', array($email));
		$changemode->password = password_hash($res_pass, PASSWORD_DEFAULT);
		R::store($changemode);
		mail($email, 'New password', $res_pass);
		$done[] = "If you have not received your message, see the spam section!";
	}

if ( !empty($errors) )
{
  echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
}
if ( !empty($done) )
{
  echo '<div style="color: green;">'.array_shift($done).'</div><hr>';
}
}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Document</title>
		<link rel="stylesheet" href="../style/reg_log.css">
		<script src="../js/reg_log.js"></script>
	</head>
  	<body>
	    <div class="l-form">
			<form action="reset.php" method="post" class="form" id="form">
        		<h1 class="font__title">Reset Password</h1>
				<div class="form__div">
					<input type="email" class="form__input" placeholder=" " name="email">
					<label for="" class="form__label">Email</label>
		        </div>
				<div style="display: flex;" class="p">
				<p style="    margin-left: 10px;"><span><a sty href="login.php">Back</a></span></p>
		        </p>
		        <p>
					<button style="    margin: -10px 5px 10px 35px;" type="submit" class="form__button" name="send_mes">Send new password</button>
					</p>
				</div>
			</form>
		</div>
	</body>
</html>