<?php
	require ($_SERVER["DOCUMENT_ROOT"] . '/Ali/db.php');

	$data = $_POST;
	if ( isset($data['do_signup']) )
	{
		$errors = array();
		if ( trim($data['login']) == '' )
		{
			$errors[] = 'Enter your username!';
		}
		if ( trim($data['email']) == '' )
		{
			$errors[] = 'Enter your email!';
		}
		if ( $data['password'] == '' )
		{
			$errors[] = 'Enter your password!';
		}else if (strlen($data['password']) < 8) {
        	$errors[] = "Password too short!";
    	}else if (!preg_match("#[0-9]+#", $data['password'])) {
	        $errors[] = "Password must include at least one number!";
	    }else if (!preg_match("#[a-zA-Z]+#", $data['password'])) {
	        $errors[] = "Password must include at least one letter!";
	    }
		if ( $data['password_ch'] != $data['password'] )
		{
			$errors[] = 'Passwords are not the same!';
		}
		if ( R::count('newusers', "email = ?", array($data['email'])) > 0 )
		{
			$errors[] = 'Such an email is available!';
		}
		if ( R::count('newusers', "login = ?", array($data['login'])) > 0 )
		{
			$errors[] = 'Such a username is available!';
		}

		if ( empty($errors) )
		{
			$user = R::dispense('newusers');
			$user->login = $data['login'];
			$user->email = $data['email'];
			$user->password = password_hash($data['password'], PASSWORD_DEFAULT);
			R::store($user);
			echo '<div style="color: green;">You have successfully registered! Click this link to <a href="./login.php">login</a>!</div><hr>';
		}else
		{
			echo '<div style="color: red;     text-align: center; margin: 1%;">'.array_shift($errors).'</div><hr>';
		}

	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="../style/reg_log.css">
  <script src="../js/reg_log.js"></script>
</head>
<body>
  	<div class="l-form">
		<form action="./signup.php" method="post" class="form" id="form">
		    <h1 class="font__title">Sign Up</h1>
				
		    <div class="form__div">
				<input type="text" name="login" class="form__input" placeholder=" " value="<?php echo @$data['login'] ?>">
		    	<label for="" class="form__label">Username</label>
		  	</div>

			<div class="form__div">
				<input type="email" name="email" class="form__input" placeholder=" " value="<?php echo @$data['email'] ?>">
				<label for="" class="form__label">E-mail</label>
		  	</div>

			<div class="form__div">
				<input type="password" name="password" class="form__input" placeholder=" " value="<?php echo @$data['password'] ?>">
				<label for="" class="form__label">Password</label>
		  	</div>

			<div class="form__div">
				<input type="password" name="password_ch" class="form__input" placeholder=" " value="<?php echo @$data['password_ch'] ?>">
				<label for="" class="form__label">Confirm password</label>
		  	</div>

			<p>
				<button type="submit" class="form__button" name="do_signup">Sign up</button>
			</p>

			<p style="    margin-top: -65px;"> <br/>
				Are you <a href="login.php">registered</a> ?
			</p>

		</form>
	</div>
</body>
</html>