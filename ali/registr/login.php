<?php
	require ($_SERVER["DOCUMENT_ROOT"] . '/Ali/db.php');

  $data = $_POST;
  if (isset($data['do_login']))
  {
    $errors = array();
    $user = R::findOne('newusers','login = ?',array($data['login']));
    if ($user)
    {
      if (password_verify($data['password'], $user->password))
      {
        $_SESSION['logged_user'] = $user;
        echo '<div style="color: green;">Login completed successfully! You can go to the main page with this <a href="../index.php">link</a>!</div><hr>';
      }else{
        $errors[] = 'Password typed incorrectly!';
      }
    }else
    {
      $errors[] = 'No such username found!';
    }

    if ( !empty($errors) )
    {
      echo '<div  style="color: red; text-align: center ; margin: 1%;">'.array_shift($errors).'</div><hr>';
    }

  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../style/reg_log.css">
    <script src="../js/reg_log.js"></script>
  </head>
  <body>
    <div class="l-form">
      <form action="login.php" method="post" class="form" id="form">
        <h1 class="font__title">Sign In</h1>
        
        <div class="form__div">
          <input type="text" name="login" class="form__input" placeholder=" "  value="<?php echo @$data['login'] ?>">
          <label for="" class="form__label">Username</label>
        </div>

        <div class="form__div">
          <input type="password" name="password" class="form__input" placeholder=" "  value="<?php echo @$data['password'] ?>">
          <label for="" class="form__label">Password</label>
        </div>

        <p>
          <span><a href="signup.php">Are you not registered ?</a></span><button style="margin-top: -28px;" type="submit" class="form__button" name="do_login">Sign in</button>
        </p>

        <p><br/>
          <a style="    margin-left: 30px;" href="reset.php">Did you forget your password ?</a>
        </p>
      </form>
    </div>
  </body>
</html>